package com.sh.api.demo.service.impl;

import com.github.pagehelper.PageHelper;
import com.sh.api.demo.dao.UserMapper;
import com.sh.api.demo.model.User;
import com.sh.api.demo.service.UserService;
import com.sh.api.core.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import com.github.pagehelper.PageInfo;

import java.util.List;


/**
 * Created by ht_CodeGenerator on 2020/04/12.
 */
@Service
@Transactional(readOnly = true)
public class UserServiceImpl extends AbstractService<User> implements UserService {
    @Resource
    private UserMapper tUUserMapper;

    @Override
    public User findDetailByInput(User input) {
        return tUUserMapper.selectOne(input);
    }
    @Override
    public PageInfo findListByInput(User input) {
            PageHelper.startPage(1, 10);
            List<User>  list = tUUserMapper.selectAll();
            PageInfo pageInfo = new PageInfo(list);
            return pageInfo;
    }
}