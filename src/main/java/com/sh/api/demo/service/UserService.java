package com.sh.api.demo.service;
import com.sh.api.demo.model.User;
import com.sh.api.core.Service;
import com.github.pagehelper.PageInfo;


/**
 * Created by ht_CodeGenerator on 2020/04/12.
 */
public interface UserService extends Service<User> {
   public User findDetailByInput(User input);
   public PageInfo findListByInput(User input);
}
