package com.sh.api.demo.web;
import com.sh.api.core.Result;
import com.sh.api.core.ResultGenerator;
import com.sh.api.demo.model.User;
import com.sh.api.demo.service.UserService;
import com.github.pagehelper.PageInfo;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;
import com.sh.log.aop.SaveLog;

/**
* Created by ht_CodeGenerator on 2020/04/12.
*/
@RestController
@RequestMapping("/user")
public class UserController {
    @Resource
    private UserService userService;

    @PostMapping(value="/detail",consumes=MediaType.APPLICATION_JSON_VALUE)
    @SaveLog
    public Result detail(@RequestBody @Valid  User input) {
        User user = userService.findDetailByInput(input);
        return ResultGenerator.genSuccessResult(user);
    }

    @PostMapping(value="/list",consumes=MediaType.APPLICATION_JSON_VALUE)
    @SaveLog
    public Result list(@RequestBody @Valid  User input) {
        PageInfo pageInfo = userService.findListByInput(input);
        return ResultGenerator.genSuccessResult(pageInfo);
    }
}
