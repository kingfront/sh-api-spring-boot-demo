package com.sh.api.core;

/**
 * 响应码枚举，参考HTTP状态码的语义
 */
public enum ResultCode {
    SUCCESS(200),//成功
    FAIL(400),//失败
    PARAM_ERROR(101),//参数校验错误
    UNAUTHORIZED(401),//未认证（签名错误）
    NOT_FOUND(404),//接口不存在
    INTERNAL_SERVER_ERROR(500);//服务器内部错误

    private final int ret;

    ResultCode(int ret) {
        this.ret = ret;
    }

    public int ret() {
        return ret;
    }
}
