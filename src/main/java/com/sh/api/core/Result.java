package com.sh.api.core;

import com.alibaba.fastjson.JSON;

/**
 * 统一API响应结果封装
 */
public class Result<T> {
    private int ret;
    private String message;
    private T data;

    public Result setCode(ResultCode resultCode) {
        this.ret = resultCode.ret();
        return this;
    }

    public int getRet() {
        return ret;
    }

    public String getMessage() {
        return message;
    }

    public Result setMessage(String message) {
        this.message = message;
        return this;
    }

    public T getData() {
        return data;
    }

    public Result setData(T data) {
        this.data = data;
        return this;
    }

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }
}
