package com.sh.api.generator;

import static com.sh.api.core.ProjectConstant.BASE_PACKAGE;

/**
 * 项目常量
 */
public final class ProjectConstantG {
    public static final String PROJECT_NAME = "shdemo";//项目名称占位符
    public static final String PROJECT_NAME_PACKAGE = "." + PROJECT_NAME;//项目包名
    public static final String PROJECT_BASE_PACKAGE = BASE_PACKAGE + PROJECT_NAME_PACKAGE;//项目基本包
    public static final String MODEL_PACKAGE = PROJECT_BASE_PACKAGE + ".model";//生成的Model所在包
    public static final String MAPPER_PACKAGE = PROJECT_BASE_PACKAGE + ".dao";//生成的Mapper所在包
    public static final String SERVICE_PACKAGE = PROJECT_BASE_PACKAGE + ".service";//生成的Service所在包
    public static final String SERVICE_IMPL_PACKAGE = SERVICE_PACKAGE + ".impl";//生成的ServiceImpl所在包
    public static final String CONTROLLER_PACKAGE = PROJECT_BASE_PACKAGE + ".web";//
}
