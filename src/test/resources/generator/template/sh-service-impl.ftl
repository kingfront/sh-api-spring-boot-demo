package ${baseProjectPackage}.service.impl;

import ${baseProjectPackage}.dao.${modelNameUpperCamel}Mapper;
import ${baseProjectPackage}.model.${modelNameUpperCamel};
import ${baseProjectPackage}.service.${modelNameUpperCamel}Service;
import ${basePackage}.core.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import com.github.pagehelper.PageInfo;

import java.util.List;


/**
 * Created by ${author} on ${date}.
 */
@Service
@Transactional(readOnly = true)
public class ${modelNameUpperCamel}ServiceImpl extends AbstractService<${modelNameUpperCamel}> implements ${modelNameUpperCamel}Service {
    @Resource
    private ${modelNameUpperCamel}Mapper ${modelNameLowerCamel}Mapper;

    @Override
    public ${modelNameUpperCamel} findDetailByInput(${modelNameUpperCamel} input) {
        return ${modelNameLowerCamel}Mapper.selectOne(input);
    }
    @Override
    public PageInfo findListByInput(${modelNameUpperCamel} input) {
            PageHelper.startPage(1, 10);
            List<${modelNameUpperCamel}>  list = ${modelNameLowerCamel}Mapper.selectAll();
            PageInfo pageInfo = new PageInfo(list);
            return pageInfo;
    }
}