package ${baseProjectPackage}.service;
import ${baseProjectPackage}.model.${modelNameUpperCamel};
import ${basePackage}.core.Service;
import com.github.pagehelper.PageInfo;


/**
 * Created by ${author} on ${date}.
 */
public interface ${modelNameUpperCamel}Service extends Service<${modelNameUpperCamel}> {
   public ${modelNameUpperCamel} findDetailByInput(${modelNameUpperCamel} input);
   public PageInfo findListByInput(${modelNameUpperCamel} input);
}
