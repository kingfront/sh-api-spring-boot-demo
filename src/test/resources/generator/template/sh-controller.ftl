package ${baseProjectPackage}.web;
import ${basePackage}.core.Result;
import ${basePackage}.core.ResultGenerator;
import ${baseProjectPackage}.model.${modelNameUpperCamel};
import ${baseProjectPackage}.service.${modelNameUpperCamel}Service;
import com.github.pagehelper.PageInfo;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;
import com.sh.log.aop.SaveLog;

/**
* Created by ${author} on ${date}.
*/
@RestController
@RequestMapping("${baseRequestMapping}")
public class ${modelNameUpperCamel}Controller {
    @Resource
    private ${modelNameUpperCamel}Service ${modelNameLowerCamel}Service;

    @PostMapping(value="/detail",consumes=MediaType.APPLICATION_JSON_VALUE)
    @SaveLog
    public Result detail(@RequestBody @Valid  ${modelNameUpperCamel} input) {
        ${modelNameUpperCamel} ${modelNameLowerCamel} = ${modelNameLowerCamel}Service.findDetailByInput(input);
        return ResultGenerator.genSuccessResult(${modelNameLowerCamel});
    }

    @PostMapping(value="/list",consumes=MediaType.APPLICATION_JSON_VALUE)
    @SaveLog
    public Result list(@RequestBody @Valid  ${modelNameUpperCamel} input) {
        PageInfo pageInfo = ${modelNameLowerCamel}Service.findListByInput(input);
        return ResultGenerator.genSuccessResult(pageInfo);
    }
}
